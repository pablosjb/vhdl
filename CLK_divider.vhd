----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:56:04 12/29/2015 
-- Design Name: 
-- Module Name:    CLK_divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CLK_divider is
	 generic ( relacion: integer :=10000000);
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clk_out : out  STD_LOGIC);
end CLK_divider;

architecture Behavioral of CLK_divider is

	signal count: integer range 0 to relacion;
	signal se�al: STD_LOGIC := '0';
	
	begin

	process (clk , reset) 
	begin
		if reset = '1' then
			se�al <= '0';
			count <= 0;
		elsif clk = '1' and clk'event then
			if (count = relacion) then
				se�al <= not(se�al);
				count <= 0;
			else
				count <= count + 1;
			end if;
		end if;
	end process;
	
	clk_out <= se�al ;
	
end Behavioral;

