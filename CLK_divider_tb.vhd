LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY CLK_divider_tb IS
END CLK_divider_tb;
 
ARCHITECTURE behavior OF CLK_divider_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT CLK_divider
	 generic ( relacion : integer
				  );
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         clk_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';

 	--Outputs
   signal clk_out : std_logic;

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut_1Hz: CLK_divider 
	GENERIC MAP (relacion => (50000000/(2*1))-1
					)
	PORT MAP (
          clk => clk,
          reset => reset,
          clk_out => clk_out
        );
	
 clk <= not clk after 10 ns;
 


   -- Stimulus process
 process
   begin		
      -- hold reset state for 100 ns.
      wait for 1500 ms;	
		reset <= '1';
		wait for 1900 ms;
		reset <= '0';
      

      -- insert stimulus here 

      wait;
   end process;

END;
